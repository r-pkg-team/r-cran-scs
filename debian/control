Source: r-cran-scs
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-scs
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-scs.git
Homepage: https://cran.r-project.org/package=scs
Rules-Requires-Root: no

Package: r-cran-scs
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Splitting Conic Solver
 Solves convex cone programs via operator splitting. Can solve: linear
 programs ('LPs'), second-order cone programs ('SOCPs'), semidefinite
 programs ('SDPs'), exponential cone programs ('ECPs'), and power cone
 programs ('PCPs'), or problems with any combination of those cones.
 'SCS' uses 'AMD' (a set of routines for permuting sparse matrices prior
 to factorization) and 'LDL' (a sparse 'LDL' factorization and solve
 package) from 'SuiteSparse' (<http://www.suitesparse.com>).
